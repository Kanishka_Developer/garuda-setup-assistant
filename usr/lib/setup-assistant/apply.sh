#!/bin/bash
if [ -e "$1" ]; then
	echo "Running pre-installation stuff.."
	echo ""
	sudo bash - <$1
fi


echo ""
echo "Installing packages.."
echo ""
sudo pacman --needed -S $(cat - <$2)

if [ -e "$3" ]; then
	echo ""
	echo "Enabling services.."
	echo ""
	sudo bash - <$3
fi

echo ""
read -p "Press enter to finish"
